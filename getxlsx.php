<?php
$fileName = $_GET['file_name'];

if (empty($fileName)) {

  http_response_code(404);

} else {

  $fileNameWithExt = $fileName . '.xls';
  $filePath = $_SERVER['DOCUMENT_ROOT'] . '/xls/' . $fileNameWithExt;

  if (!file_exists($filePath)) {

    http_response_code(404);

  } else {

    $data = file_get_contents($filePath);

    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename=' . $fileNameWithExt);

    echo $data;
  }
}